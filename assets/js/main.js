$(function () {
    //BEGIN MENU SIDEBAR
    $('#sidebar').css('min-height', '100%');
    $('#side-menu').metisMenu();

    $(window).bind("load resize", function () {
        if ($(this).width() < 768) {
            $('div.sidebar-collapse').addClass('collapse');
        } else {
            $('div.sidebar-collapse').removeClass('collapse');
            $('div.sidebar-collapse').css('height', 'auto');
        }
        if($('body').hasClass('sidebar-icons')){
            $('#menu-toggle').hide();
        } else{
            $('#menu-toggle').show();
        }
    });
    //END MENU SIDEBAR
    


    $('.option-demo').hover(function() {
        $(this).append("<div class='demo-layout animated fadeInUp'><i class='fa fa-magic mrs'></i>Demo</div>");
    }, function() {
        $('.demo-layout').remove();
    });


    $('#header-topbar-page .demo-layout').live('click', function() {
        var HtmlOption = $(this).parent().detach();
        $('#header-topbar-option-demo').html(HtmlOption).addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass('animated flash');
        });
        $('#header-topbar-option-demo').find('.demo-layout').remove();
        return false;
    });
    $('#title-breadcrumb-page .demo-layout').live('click', function() {
        var HtmlOption = $(this).parent().html();
        $('#title-breadcrumb-option-demo').html(HtmlOption).addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass('animated flash');
        });
        $('#title-breadcrumb-option-demo').find('.demo-layout').remove();
        return false;
    });
    // CALL FUNCTION RESPONSIVE TABS
    //BEGIN CHECKBOX & RADIO
  /*  if($('#demo-checkbox-radio').length <= 0){
        $('.icheck-check-btn').iCheck({
            checkboxClass: 'icheckbox_minimal-grey',
            increaseArea: '20%' // optional
        });
        $('.icheck-radio-btn').iCheck({
            radioClass: 'iradio_minimal-grey',
            increaseArea: '20%' // optional
        });
    }
    //END CHECKBOX & RADIO
    $('#userList input#selectAll').on('ifClicked', function(event){
         $('#userList input[name=userId\\[\\]]').iCheck('toggle');
    });
   */
});

$(document).on('change', '#toggle-trigger', function(e){

    if ($(this).is(':checked')) {
                   $('.advanced-mode').show();
                   $('.simple-mode').hide();
        }else{
               $('.advanced-mode').hide();
                   $('.simple-mode').show(); 
        }
 });
 $('.advanced-mode').hide();
 //$('[data-submenu]').submenupicker();
$('.radiogroup').on('click', function(){
             $(this).parent().find(".active").removeClass('active');
            $(this).addClass("active");
});
$(".switch-another-sub-category .sub-vehicles").each(function() {

});



