

$(document).ready(function(){

  tinymce.init({ 
  	selector:'textarea.tinymce',
  	theme:  'modern',
    skin: "lightgray",
    height: 500,
  	plugins: ["advlist autolink link image lists charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
              "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
              ],
              toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
              toolba2:  "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor | print preview code | caption ",
              image_caption: true,
              image_advtab : true,
              
              // file manager path
              external_filemanager_path: base_url+"assets/js/filemanager/",
              filemanager_title: "myFileManager",
              external_plugins: {"filemanager": base_url+"assets/js/filemanager/plugin.min.js"},
              visualblocks_default_autohide: true,
              style_formats_autohide: true,
              style_formats_merge: true,

  });
});
 
