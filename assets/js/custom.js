(function(){ /* start iife */

     var social = $(".footer-nav-item");
             console.log(social);
            
            social.on("mouseover",function(){
                $(this).siblings().find("a").css({color:"gray"});
            });
            
             social.on("mouseout",function(){
                $(this).siblings().find("a").css({color:"black"});
                
            });

    /* start contact form function */
    $("#submit-contact").on("click",function(e){
        e.preventDefault();


        var url =base_url+"contact/send_email";
        var vars = {
            fname:   $("#firstName").val(),
            lname:   $("#lastName").val(),
            email:   $("#emailAddress").val(),
            subject: $("#emailSubject").val(),
            message: $("#emailMessage").val()
        };

        $.ajax({
            url: url,
            data: vars,
            type: "POST",
            dataType: "json",
            success: function(data){
                /* fail */
                if(data.status==0){
                    $("#notify")
                        .css({"display":"block"})
                        .html(data.message);n
                }
                /* success */
                if(data.status==1){
                    $("#notify")
                        .css({"display":"block"}).addClass("success")
                        .html(data.message);
                    setTimeout(function(){
                        $("#notify").css({"display":"none"}).removeClass("success");
                    },4000);
                }
                /* validation errors */
                if(data.status == -1){
                    $("#notify")
                        .css({"display":"block"})
                        .html(data.message);
                }

            }
        });

        /* end contact form function */
    });

 /* end iife */
})();