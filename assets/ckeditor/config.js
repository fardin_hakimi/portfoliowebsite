﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

	// %REMOVE_START%
	// The configuration options below are needed when running CKEditor from source files.
	config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,copyformatting,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,undo,wsc,lineutils,widgetselection,widget,codesnippet,filetools,notification,notificationaggregator,uploadwidget,uploadfile';
	config.skin = 'moono-lisa';
	// %REMOVE_END%

	config.filebrowserBrowseUrl = 'assets/ckfinder/browse.php?opener=ckeditor&type=files';
   config.filebrowserImageBrowseUrl = 'assets/ckfinder/browse.php?opener=ckeditor&type=images';
   config.filebrowserFlashBrowseUrl = 'assets/ckfinder/browse.php?opener=ckeditor&type=flash';
   config.filebrowserUploadUrl = 'assets/ckfinder/upload.php?opener=ckeditor&type=files';
   config.filebrowserImageUploadUrl = 'assets/ckfinder/upload.php?opener=ckeditor&type=images';
   config.filebrowserFlashUploadUrl = 'assets/ckfinder/upload.php?opener=ckeditor&type=flash';

	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
