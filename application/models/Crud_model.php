<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function do_upload($file_name = NULL, $dir = NULL){
        $config['upload_path'] = $dir . '/';
        $config['allowed_types'] = 'jpg|png|gif|jpeg';
        $config['max_size'] = '10000000000';
        $config['overwrite'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_name)) {
            return false;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    public function get_subscriber_email($start = NULL, $end = NULL) {
        $sql = "SELECT * FROM `subscribers`  limit $start, $end ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function deletePic($file_name = NULL, $dir = NULL){
        unlink(FCPATH.$dir.$file_name);
    }

    public function reupload_file($doc, $a) {
        $b = explode(".", $a);
        $config['upload_path'] = 'doc/';
        $config['allowed_types'] = 'pdf|doc|docx|txt';
        $config['max_size'] = '10000000000';
        if (preg_match('/_ver/', $a)) {
            
        } else {
            $config['file_name'] = $b[0] . '_ver';
        }
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($doc)) {
            return false;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    public function upload_pic($image, $dir = NULL) {
        $config['upload_path'] =  $dir . '/';
        $config['allowed_types'] = 'jpg|png|gif|svg';
        $config['overwrite'] = FALSE;
        $config['max_size'] = '10000000000';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            return false;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    public function html_form_dropdown($fields = NULL, $table = NULL, $option = NULL, $field_id = NULL, $field_name = NULL,$where = NULL, $order_by_field = NULL, $order_by = NULL) {
        $this->db->select($fields);
        $this->db->distinct();
		if(!empty($where)){
					$this->db->where($where);
		}
        if ($order_by_field != '' && $order_by != '') {
            $this->db->order_by($order_by_field, $order_by);
        }	
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            $data = array();
            if(!empty($option)){
                 $data= array('0' => $option);
                  // $data[''] = $option;
            }
            foreach ($query->result() as $row) {
                $data[$row->$field_id] = $row->$field_name;
            }
            return $data;
        }
    }

    public function insert_record($table, $data){
        
        $this->db->trans_start();
        $query = $this->db->insert($table, $data);
        $this->db->trans_complete();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
       public function insert_record_get_last_id($table, $data) {
        $this->db->trans_start();
        $query = $this->db->insert($table, $data);
        $id= $this->db->insert_id();
        $this->db->trans_complete();
        if ($query) {
            return $id;
        } else {
            return false;
        }
    }
    
    
    public function update_record($table, $where, $data) {
        $this->db->trans_start();
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        
        $this->db->trans_complete();

       
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function fetch_singleobj($fields = NULL, $table = NULL, $where = NULL) {
        $this->db->select($fields)->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function count_singleobj($fields = NULL, $table = NULL, $where = NULL) {
        $this->db->select($fields)->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
    }

    public function getLastInserted() {
        return $this->db->insert_id();
    }

    public function remove_record($table = NULL, $where = NULL) {
        $this->db->trans_start();
        $this->db->where($where);
        $query = $this->db->delete($table);
        $this->db->trans_complete();
        return $query;
    }

    public function remove_record_or_where($table = NULL, $where = NULL) {
        $this->db->trans_start();
        $this->db->or_where($where);
        $query = $this->db->delete($table);
        $this->db->trans_complete();
        return $query;
    }


    public function get_all($fields = NULL, $table = NULL, $where = NULL, $limit = NULL,$group = NULL, $order_by_field = NULL, $order_by = NULL) {
        $this->db->select($fields)->from($table);
        if (!empty($where)) {
            $this->db->where($where);
            // print_r($this->db->last_query()) ;
        }

        if(!empty($group)){
        $this->db->group_by($group); 
    }

        if ($order_by_field != '' && $order_by != '') {
            $this->db->order_by($order_by_field, $order_by);
        }
        if (!empty($limit)) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result();
        }
    }

    public function get_all_where_or($fields = NULL, $table = NULL, $where = NULL, $limit = NULL, $order_by_field = NULL, $order_by = NULL) {
        $this->db->select($fields)->from($table);
        if (!empty($where)) {
            $this->db->or_where($where);
        }
        if ($order_by_field != '' && $order_by != '') {
            $this->db->order_by($order_by_field, $order_by);
        }
        if (!empty($limit)) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function get_all_custm_pkg($fields = NULL, $table = NULL, $where = NULL, $limit = NULL, $order_by_field = NULL, $order_by = NULL) {
        $this->db->select($fields)->from($table);
        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($order_by_field != '' && $order_by != '') {
            $this->db->order_by($order_by_field, $order_by);
        }
        if (!empty($limit)) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function get_where($fields = NULL, $table = NULL, $where = NULL) {
        $this->db->select($fields);
        $this->db->where($where);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function get_where_num_row($fields, $table, $where) {
        $this->db->select($fields);
        $this->db->where($where);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_where_in($table = NULL, $where_in = NULL, $where_in_idz = NULL) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where_in($where_in, $where_in_idz);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function html_form_dropdown_where_in($fields = NULL, $table = NULL, $option = NULL, $field_id = NULL, $field_name = NULL, $where_in = NULL, $where_in_idz = NULL) {
        $this->db->select($fields);
        $this->db->distinct();
        
        if(!empty($where_in)){
                     
                    $this->db->where_in($where_in, $where_in_idz);
        }   
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            $data = array();
            if(!empty($option)){
                $data= array('0' => $option);
                   //$data[''] = $option;
            }
            foreach ($query->result() as $row) {
                $data[$row->$field_id] = $row->$field_name;
            }
            return $data;
        }
    }

    public function get_where_like($fields, $table = NULL, $where = NULL, $order_by_field = NULL, $order_by = NULL) {
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->or_like($where);
        if ($order_by_field != '' && $order_by != '') {
            $this->db->order_by($order_by_field, $order_by);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
/*	public function joins($fields=NULL, $table=NULL, $joinqueries=NULL, $where=NULL, $mode,$where_in = NULL, $where_in_idz = NULL,$distinct_col=NULL)
	{
	$this->db->select($fields);
	$this->db->from($table);
	if(!empty($distinct_col))
	{
				  $this->db->group_by($distinct_col);
	}			  
	if(!empty($joinqueries)){
			foreach ($joinqueries as $joinquery):
			if(isset($joinquery['type']))
			{
				  $this->db->join($joinquery['table'],$joinquery['where'],$joinquery['type'] );
			}
			else{
			
				$this->db->join($joinquery['table'],$joinquery['where']);
			}
			    
			endforeach;
	}
	if(!empty($where) && $mode == "row"){
		 $this->db->where($where);	
		 $query = $this->db->get();
	     $fetch_mode = $query->row();
	}
	else if($where != "" && $where_in!="" && $where_in_idz!=""){
		 $this->db->where_in($where_in, $where_in_idz);
		 $this->db->where($where);	
		 $query = $this->db->get();
	     $fetch_mode = $query->result();
	}
	else if(!empty($where)){
		
		$this->db->where($where);	
		$query = $this->db->get();
		$fetch_mode = $query->result();
	}
	else{
		$query = $this->db->get();
		$fetch_mode = $query->result();
	}
	    // $query = $this->db->get();
	     $result = $fetch_mode;
		  return $result;
	}
	*/
	public function orLike($fields = NULL,$table = NULL, $field_id= NULL,$field_name= NULL,$option = NULL,$order_by_field = NULL, $order_by = NULL, $where = NULL){
    $sql   = "SELECT $fields FROM $table  WHERE $field_id LIKE '%$field_name%' ";
 
	if(!empty($where)){
		$sql   .="AND $where";
	}
	if($option != ""){
		$sql   .=" AND $option";
	}
	if ($order_by_field != '' && $order_by != '') {
        $sql   .=" ORDER BY $order_by_field $order_by";
    }
     $query = $this->db->query($sql);
    if($query->num_rows > 0){
       return $query->result();
    }
    }
	
	
	public function get_join($fields=NULL, $table=NULL, $joinqueries=NULL, $type = NULL,$mode = NULL, $where=NULL,$group=NULL,$order=NULL,$limit = NULL){

	$this->db->select($fields);
	$this->db->from($table);
	if(!empty($joinqueries)){

			foreach ($joinqueries as $joinquery):

			if(isset($joinquery['type']))
			{

				$this->db->join($joinquery['table'],$joinquery['where'],$joinquery['type'] );
			}
			else{
				
				$this->db->join($joinquery['table'],$joinquery['where']);
			}
			    
			endforeach;
			
	if($type == "like" && $mode == "row"){
		 $this->db->like($where); 
	}else if($type == "or_like" && $mode == "row"){
		 $this->db->or_like($where); 
	}else if($type == "not_like" && $mode == "row"){
		 $this->db->not_like($where); 
	}else if($type == "or_not_like" && $mode == "row"){
		 $this->db->or_not_like($where); 
	}else if($type == "like" && $mode == "result"){
		 $this->db->like($where); 
	}else if($type == "or_like" && $mode == "result"){
		 $this->db->or_like($where); 
	}else if($type == "not_like" && $mode == "result"){
		 $this->db->not_like($where); 
	}else if($type == "or_not_like" && $mode == "result"){
		 $this->db->or_not_like($where); 
	}else if($type == "where" && $mode == "row"){
		 $this->db->where($where); 
	}else if($type == "or_where" && $mode == "row"){
		 $this->db->or_where($where); 
	}else if($type == "where_in" && $mode == "row"){
		 $this->db->where_in($where); 
	}else if($type == "or_where_in" && $mode == "row"){
		 $this->db->or_where_in($where); 
	}else if($type == "where_not_in" && $mode == "row"){
		 $this->db->where_not_in($where); 
	}else if($type == "or_where_not_in" && $mode == "row"){
		 $this->db->or_where_not_in($where); 
	}else if($type == "where" && $mode == "result"){
		 $this->db->where($where); 
	}else if($type == "or_where" && $mode == "result"){
		 $this->db->or_where($where); 
	}else if($type == "where_in" && $mode == "result"){
         $values = array_values($where);
		 $this->db->where_in(key($where),$values[0]); 
    
	}else if($type == "or_where_in" && $mode == "result"){
		 $this->db->or_where_in($where); 
	}else if($type == "where_not_in" && $mode == "result"){
		 $this->db->where_not_in($where); 
	}else if($type == "or_where_not_in" && $mode == "result"){
		 $this->db->or_where_not_in($where); 
	}
	
	if(!empty($group)){
		$this->db->group_by($group); 
	}
	if(!empty($order)){
		$this->db->order_by($order); 
	}
	if(!empty($limit)){
		$this->db->limit($limit);
	}
	if($mode == "row"){
		 $query = $this->db->get();
	     $fetch_mode = $query->row();

	}else{
		$query = $this->db->get();
	     $fetch_mode = $query->result();

	}
       $result = $fetch_mode;
	   return $result;
	}
		    
	}  
}

?>