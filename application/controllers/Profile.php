<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller{

public function __construct(){
      parent::__construct();
      $this->load->library('pro');
      $this->load->model('crud_model');

        if(@$this->session->userdata['admin']['loggedIn'] ==FALSE){
              redirect(base_url().'admin');
           exit;
          }
    }

    /* profile security start */
public function security (){

    $adminId = $this->session->userdata['admin']['user_id'];

	$where = array('Id' => $adminId);
	$tableName = "Profile";
	$data["Admin"] = $this->crud_model->fetch_singleobj("*",$tableName,$where);



	$this->loadResources();
	$this->load->view("admin/profile",$data);
	$this->loadFooter();
}
/* profile security end */

/* start update profile security */
public function update_security(){

 /* load validation library */
 $this->load->library('form_validation');


$this->form_validation->set_rules('userName','username','trim|required|valid_email',
  array('required'=> '%s is required', 'valid_email'=> '%s is incorrect, You must enter a valid email address'));

     /* validate password */
 $this->form_validation->set_rules('password','Password','trim|required|min_length[8]',
 array('required'=> '%s is required'));


if($this->form_validation->run() == true){

	$newUserName = $this->input->post("userName");
	$newPassword = md5($this->input->post("password")); 
	$adminId = $this->session->userdata['admin']['user_id'];
	$data    = array('User_Name' => $newUserName,
		             'Password'  => $newPassword
		);
	$where = array('Id' => $adminId);
	$result = $this->crud_model->update_record("Profile",$where,$data);

	if($result){
	redirect("Logout");
	}

}else{
	/* reload the form */
	$this->security();
}

}/* end update profile security */

    /* start profile skills */

    public function skills(){

        // get all skills from db
        $data["skills"] = $this->crud_model->get_all("*","Skills");

        $this->loadResources();
        $this->load->view("admin/skills",$data);
        $this->loadFooter();

    }

    public function add_skill(){

        // get all skills from db
        $data["skills"] = $this->crud_model->get_all("*","Skills");

        $this->loadResources();
        $this->load->view("admin/add-skill",$data);
        $this->loadFooter();

    }


    /* start create skill */

    public function create_skill(){

        /* set data */

        $title = $this->input->post("skillTitle");
        $description = $this->input->post("description");


        $skill = array(
            'Name' =>$title ,
            'Description' =>$description
        );

        $tableName = "Skills";
        $result ="";
        /* end set data  */


        /* check if we are in update mode or add mode, if in edit mode delete the picture */
        if(!empty($this->input->post('skillId')) AND !empty($_FILES['featuredImage']['name']) ){
            $this->pro->delete_photo($this->input->post('skillId'),$tableName);
        }

        /* adding featured image */
        if(!empty($_FILES['featuredImage']['name'])){
            /* upload to file system */
            $featured  = $this->crud_model->do_upload('featuredImage','assets/images');
            $file_name = $featured['upload_data']['file_name'];
            $skill['Featured_image'] = $file_name;
        }
        /* end */

        // A new skill
        if($this->input->post('submit-skill') =="Add"){

            $result =$this->crud_model->insert_record($tableName, $skill);

        }else if($this->input->post('submit-skill') =="Edit"){

            // insert record into the given table
            $where = array('Skills.Id' =>$this->input->post('skillId'));
            $result = $this->crud_model->update_record($tableName, $where, $skill);
        }

        if(!empty($result)){
            redirect('Profile/skills');
        }

        /* update */

    }
    /* end create skill */


    /* start edit skill */


    public function edit_skill($Id=null){

        if (empty($Id)){
            $Id= $this->uri->segment(3);
        }

        $data=[];

        /* if  Id is not empty fetch record */
        if(!empty($Id)){
            $where = array('Skills.Id' =>$Id);
            $data["skill"]= $this->crud_model->fetch_singleobj("*","Skills",$where);
        }


        $this->loadResources();
        $this->load->view('admin/add-skill',$data);
        $this->loadFooter();
    }

    /* end edit skill */

    /* start view skill details  */
    public function view_skill(){

        $Id=$this->uri->segment(3);

        $where= array('Skills.Id' =>$Id);
        $data["row"]=$this->crud_model->fetch_singleobj("*","Skills",$where);

        $this->loadResources();
        $this->load->view('admin/skill-details',$data);
        $this->loadFooter();

    }
    /* end view skill details  */

    /* start delete skill  */
    public function delete_skill(){
        $skillId=$this->uri->segment(3);
        $tableName = "Skills";
        // delete associated image from file system
        $this->pro->delete_photo($skillId,$tableName);
        $where = array('Skills.Id' => $skillId);
        $result = $this->crud_model->remove_record($tableName,$where);

        if($result){
            redirect('Work');
        }
    }
    /* end */

    /* about */
    public function about(){

        $adminId = $this->session->userdata['admin']['user_id'];

        $where = array('Id' => $adminId);
        $tableName = "Profile";
        $data["Admin"] = $this->crud_model->fetch_singleobj("*",$tableName,$where);

        $this->loadResources();
        $this->load->view('admin/about',$data);
        $this->loadFooter();
    }
/* end about */


    /* start update about */
    public function update_about(){

        /* load validation library */
        $this->load->library('form_validation');


        $this->form_validation->set_rules('content','Content','trim|required',
            array('required'=> '%s is required'));


        if($this->form_validation->run() == true){

            $content = $this->input->post("content");
            $adminId = $this->session->userdata['admin']['user_id'];
            $data    = array('About' => $content
            );

            /* adding featured image */
            if(!empty($_FILES['featuredImage']['name'])){
                /* delete previous picture */
                $this->pro->delete_photo($this->session->userdata['admin']['user_id'],"Profile");
                /* upload to file system */
                $featured  = $this->crud_model->do_upload('featuredImage','assets/images');
                $file_name = $featured['upload_data']['file_name'];
                $data['Featured_image'] = $file_name;
            }
            /* end */


            $where = array('Id' => $adminId);
            $result = $this->crud_model->update_record("Profile",$where,$data);

            if($result){
                redirect("Profile/about");
            }

        }else{
            /* reload the form */
            $this->about();
        }

    }/* end update about */

    /* end profile skills */

    public function loadResources(){
    $this->load->view('includes/header-admin-top');
    $this->load->view('includes/header-admin');
    $this->load->view('includes/sidebar');
  }

  public function loadFooter(){
    $this->load->view('includes/admin-footer');
  }


}
    ?>