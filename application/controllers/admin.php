<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class admin extends CI_Controller {
	/* constructor */
 public function __construct(){
 	/* call parent constructor */
      parent::__construct();
}
	public function index(){
		$this->load->view('includes/links');
		$this->load->view('admin/login');
	}


public function authorizeUser(){
   	 $this->load->library('form_validation');

     if($this->input->post('signin')){
     	/* validate email address */
 
 $this->form_validation->set_rules('emailAddress','Email Address','trim|required|valid_email',
  array('required'=> '%s is required, Please check your credentials and try again.', 'valid_email'=> '%s is incorrect, Please check your credentials and try again.'));

     /* validate password */
 $this->form_validation->set_rules('password','Password','trim|required|min_length[8]',
 array('required'=> '%s is required, Please check your credentials and try again.'));


/* if validation was successfull */
if($this->form_validation->run() == true){

/* get fields data */
				$userName = $this->input->post("emailAddress");
				$password =md5($this->input->post("password"));
				$tableName = "Profile";


				$where = array('User_Name' => $userName ,
					           'Password'  =>$password
				 );

/* check if we find a match */

				$result = $this->crud_model->fetch_singleobj("*",$tableName,$where);

/* if found */
				if($result){

					$session = array(
					'user_name'			=>$result->User_Name,
					'user_id'			=>$result->Id,
					'Featured_image'    =>$result->Featured_image,
					'about'             =>$result->About,
					'loggedIn'			=>TRUE
					);

					$this->session->set_userdata('admin',$session);
					 redirect(base_url().'Dashboard');

				}else{
     $this->session->set_flashdata('msg', ' Sorry, there is no match for that email address and/or password.');
			 redirect(base_url().'admin');

				}

            }else{
            	$this->index();
            }

/* whole sign in if end here */
}

/* end method */
 }

} /* end class */
?>