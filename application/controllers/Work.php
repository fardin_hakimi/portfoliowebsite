<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work extends CI_Controller{

public function __construct(){
      parent::__construct();
      $this->load->library('pro');
      $this->load->model('crud_model'); 

        if(@$this->session->userdata['admin']['loggedIn'] ==FALSE){
              redirect(base_url().'admin');
           exit;
          }
    }

	public function index()
	{
		$data =[];

    /* join categories and Works table */
  $joinqueries= array( 
                array(  'table'    =>  'Categories',
                  'where'    =>  'Works.Category_id=Categories.Id')
                    );

  $tableName = "Works";
    /* get all work items */
    $data["works"] = $this->crud_model->get_join("*, Works.Id as work_id, Works.Status as work_status, Works.Description as work_description",$tableName,$joinqueries,"","result");


		$this->loadResources();
		$this->load->view('admin/Work',$data);
		$this->loadFooter();
	}



/* start add work function */
	public function add_work(){
		$data =[];
		  // get all categories 
		$data["categories"] = $this->pro->get_all_categories();
		$this->loadResources();
		$this->load->view('admin/add-work',$data);
		$this->loadFooter();

	}

/* end add work function */



/* start create work */

	public function create_work(){

    /* set data */


        $title = $this->input->post("workTitle");
        $category_id = $this->input->post("category");
        $description = $this->input->post("description");
        $content = $this->input->post("content");


        $work = array(
        'Title' =>$title , 
        'Category_id' =>$category_id,
        'Description' =>$description,
        'Content'     =>$content,
        'Status' => 0
        );

        $tableName = "Works";
        $result ="";

/* end set data  */


/* check if we are in update mode or add mode, if in edit mode delete the picture */
if(!empty($this->input->post('workId')) AND !empty($_FILES['featuredImage']['name']) ){
 $this->pro->delete_photo($this->input->post('workId'), $tableName);
}

 /* adding featured image */
if(!empty($_FILES['featuredImage']['name'])){
/* upload to file system */
  $featured  = $this->crud_model->do_upload('featuredImage','assets/images');
  $file_name = $featured['upload_data']['file_name'];
  $work['Featured_image'] = $file_name;
           }
   /* end */


     
  // A new work
        if($this->input->post('submit-work') =="Add"){
          $work['Date_created'] =  date("Y/m/d");


       $result =$this->crud_model->insert_record($tableName, $work);

        }else if($this->input->post('submit-work') =="Edit"){

          /* delete previous featured image from file system */
         
          /* end delete */
           $work['Date_updated'] =  date("Y/m/d");


          // insert record into the given table
          $where = array('Works.Id' =>$this->input->post('workId'));
          $result = $this->crud_model->update_record($tableName, $where, $work);
        }

           if(!empty($result)){
               redirect('Work');
             } 

        /* update */
  
	}
	/* end create work */

	/* edit work */


  public function edit_work($workId=null){

    if (empty($workId)){
     $workId= $this->uri->segment(3);
    }

    $data=[];

/* if  Id is not empty fetch record */
    if(!empty($workId)){
    $where = array('Works.Id' =>$workId);
    $data["work"]= $this->crud_model->fetch_singleobj("*","Works",$where);  
    }



// get all categories 
    $data["categories"]= $this->pro->get_all_categories();
    $this->loadResources();
    $this->load->view('admin/add-work',$data);
    $this->loadFooter();
  }

	/* edit work end */



  /* start delete work status */
  public function delete_work(){
      $workId=$this->uri->segment(3);
      $tableName = "Works";
      // delete associated image from file system
      $this->pro->delete_photo($workId,$tableName);
      $where = array('Works.Id' => $workId);
      $result = $this->crud_model->remove_record($tableName,$where);

         if($result){
          redirect('Work');
                       }
  }
    /* end delete work status */


  /* start view work status */
  public function view_work(){

  $workId=$this->uri->segment(3);
  $data=[];
  $data = $this->pro->get_join_works_categories($workId);

    $this->loadResources();
    $this->load->view('admin/work-details',$data);
    $this->loadFooter();

  }

      /* update work status */
  public function update_status(){

    $workId = $this->input->post("workId");
    $status = $this->input->post("Status");

    $where = array('Works.Id' => $workId);

    $data = ($status==0) ?  array(
                    "Status" => 1 ) : array(
                    "Status" => 0
                    );
      $tablename = 'Works';  
      $this->crud_model->update_record($tablename,$where,$data); 
  }
  /* end update work status */


    public function loadResources(){
    $this->load->view('includes/header-admin-top');
    $this->load->view('includes/header-admin');
    $this->load->view('includes/sidebar');
  }

  public function loadFooter(){
    $this->load->view('includes/admin-footer');
  }
}
?>