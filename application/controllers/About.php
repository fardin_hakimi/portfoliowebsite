<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    /**
     * About constructor.
     */
    public function __construct(){
        parent::__construct();
        $this->load->model('crud_model');
    }

    public function index()
	{
	    $data=[];
	    // get all the skills from db
	    $data["skills"] = $this->get_all_skills();
	    $data["me"]  = $this->get_about_me();

		$this->load->view('includes/header');
		$this->load->view('about',$data);
		$this->load->view('includes/footer');
	}


	public function get_all_skills(){
        $results = $this->crud_model->get_all("*","Skills");
        return $results;
    }

    public function get_about_me(){
	    $result = $this->crud_model->get_all("About,Featured_image","Profile");
	    return $result[0];
    }
}
