<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

      function __construct() { 
         parent::__construct(); 
         $this->load->library('session'); 
      }

	public function index()
	{
		$this->load->view('includes/header');
		$this->load->view('contact');
		$this->load->view('includes/footer');
	}

    /* start send email function */
	public function send_email(){
    $this->load->library('form_validation');
    $data=[];

// first name
  $this->form_validation->set_rules('fname','First Name','trim|required|min_length[3]',
  array('required'=> '%s is required.', 'min_length'=> '%s must be atleast 3 characters in length.'));
// last name
  $this->form_validation->set_rules('lname','Last Name','trim|required',
  array('required'=> '%s is required.'));
// email address
  $this->form_validation->set_rules('email','Email Address','trim|required|valid_email',
  array('required'=> '%s is required', 'valid_email'=> '%s is incorrect, Please enter a valid email address.'));

// subject
  $this->form_validation->set_rules('subject','Subject','trim|required|min_length[3]',
  array('required'=> '%s is required.', 'min_length'=> '%s must be atleast 3 characters in length.'));

  // message
  $this->form_validation->set_rules('message','Message','trim|required|min_length[50]',
  array('required'=> '%s is required.', 'min_length'=> '%s must be atleast 100 characters in length.'));

  if($this->form_validation->run()==true){


    $senderName = $this->input->post("fname")." ".$this->input->post("lname");
    $senderEmail = $this->input->post("email");

        // query db for admin 

    $admin = $this->crud_model->get_all("*","Authentication");


    // get the id of the admin 
        $admin_email = $admin[0]->User_Name;
        $admin_pass = $admin[0]->Password;
    $emailSubject = $this->input->post("subject");
    $emailMessage = $this->input->post("message");

      


    $config = Array( 
        'protocol' => 'smtp', 
        'smtp_host' => 'ssl://smtp.googlemail.com', 
        'smtp_port' => 465, 
        'smtp_user' => $admin_email, // here goes your mail 
        'smtp_pass' => "code4lifework4life", // here goes your mail password 
        'mailtype' => 'html', 
        'charset' => 'iso-8859-1', 
        'wordwrap' => TRUE 
      );

      //Load email library 
         $this->load->library('email',$config); 
   
         $this->email->from($senderEmail, $senderName); 
         $this->email->to($admin_email);
         $this->email->subject($emailSubject); 
         $this->email->message($emailMessage); 


          //Send mail 
         if($this->email->send()){
          $data["message"]="Your message was recieved, I will contact you asap!";
          $data["status"]=1;

         echo json_encode($data); 
              }else{ 
          $data["message"]="something went wrong when sending your message! please try again";
          $data["status"]=0;
            echo json_encode($data); 
          }
  }else{
    $data["message"]=validation_errors();
    $data["status"]=-1;
    echo json_encode($data);
  }

	}/* end send email function */


}/* end class */
