<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	


public function __construct(){
      parent::__construct();
      $this->load->model('crud_model'); 

         if(@$this->session->userdata['admin']['loggedIn'] ==FALSE){
              redirect(base_url().'admin');
           exit;
          }
    }

public function index(){
		$this->load->view('includes/header-admin-top');
		$this->load->view('includes/header-admin');
		$this->load->view('includes/sidebar');
		$this->load->view('admin/dashboard');     
		$this->load->view('includes/admin-footer');

	}
}

?>