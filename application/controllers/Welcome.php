<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct(){
      parent::__construct();

      $this->load->model('crud_model'); 
      $this->load->library('pro'); 
    }

	public function index()
	{
		$tableName = "Works";
		$data =[];

		$data["works"] = $this->crud_model->get_all("*",$tableName);

		$this->load->view('includes/header');
		$this->load->view('Index',$data);
		$this->load->view('includes/footer');
	}


/* public view work front-end */
  public function view(){

  $workId=$this->uri->segment(2);

    $data=[];
    $data = $this->pro->get_join_works_categories($workId);
    $this->load->view('includes/header');
    $this->load->view('work-view',$data);
    $this->load->view('includes/footer');
  }
/* public view work front-end end */

}
?>
