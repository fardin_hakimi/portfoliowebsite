<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Pro{

	    protected $CI;

        // We'll use a constructor, as you can't directly call a function
        // from a property definition.
        public function __construct(){
                // Assign the CodeIgniter super-object
                $this->CI =& get_instance();
                $this->CI->load->model('crud_model');
        }


/* get all categories function */
public function get_all_categories(){
$all_categories = $this->CI->crud_model->html_form_dropdown("*","Categories", "","Id" ,"Name","");
return $all_categories;
}
/* end function */



/* start join works and categories function */
  public function get_join_works_categories($workId){

          $where = array('Works.Id' => $workId);

    /* join Categories and Works table */
     $joinqueries= array( 
                   array(  'table'    =>  'Categories',
                  'where'    =>  'Works.Category_id=Categories.Id')
                    );

     /* fetch the record */
     $data["row"]= $this->CI->crud_model->get_join("*, Works.Id as work_id, Works.Status as work_status, Works.Description as work_description","Works",$joinqueries,"where","row",$where,'','',"1");

     return $data;

  }
  /* start join works and categories function end */

    // start delete_photo function
    public function delete_photo($Id, $tableName){
        $where= array($tableName.'.Id' =>$Id);
        $row=$this->CI->crud_model->fetch_singleobj("Featured_image",$tableName,$where);
        $removable= $row->Featured_image;
        $this->CI->crud_model->deletePic($removable,"assets/images/");
    }
    // end delete photo function

}
