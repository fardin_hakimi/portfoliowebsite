<div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
                data-position="right" class="navbar-default navbar-static-side">
            <div class="sidebar-collapse menu-scroll">
                <ul id="side-menu" class="nav">
                    
                     <div class="clearfix"></div>
                    <li class="active"><a href="<?php echo base_url();?>Dashboard"><i class="fa fa-tachometer fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">Dashboard</span></a></li>



                    <!-- users start -->



                         <li>
          <a href="#">  <i class="fa fa-group fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i>

                    <span class="menu-title">Works</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>


                      
            <ul class="nav nav-second-level">
             <div class="clearfix"></div>
                 <li><a href="<?php echo base_url();?>Work">
                    <span class="menu-title">Work Management</span></a>  
                    </li>
          
            </ul>
          </li>



            <!-- users end -->


                         <li>
          <a href="#"><i class="fa fa-tags fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Catalog</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
                      
            <ul class="nav nav-second-level">
             <div class="clearfix"></div>

            </ul>
          </li>




             <li>
          <a href="#"><i class="fa fa-map-marker fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Location Management</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
                      
            <ul class="nav nav-second-level">
             <div class="clearfix"></div>
               <li>
                   <a href="<?php echo base_url();?>Locations"><span class="submenu-title">Location List</span></a>
                      </li>
                 <li>
                       <a href="<?php echo base_url();?>Locations/locationTypeList"><span class="submenu-title">Location Type List</span></a>
                      </li>

                       <li>
                       <a href="<?php echo base_url();?>doctors/doctorAccountManagement"><span class="submenu-title">Add City</span></a>
                      </li>

                       <li>
                       <a href="<?php echo base_url();?>doctors/doctorAccountManagement"><span class="submenu-title">Add Locality</span></a>
                      </li>
            </ul>
          </li>




                     <li>
          <a href="#"><i class="fa fa-university fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Doctors Board</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
                      
            <ul class="nav nav-second-level">
             <div class="clearfix"></div>
               <li>
                       <a href="<?php echo base_url();?>doctors"><span class="submenu-title">Doctors</span></a>
                      </li>
                 <li>
                       <a href="<?php echo base_url();?>doctors/doctorAccountManagement"><span class="submenu-title">doctor management demo</span></a>
                      </li>
            </ul>
          </li>



               <li>

                   <a href="#"><i class="fa fa-user fa-fw">
                           <div class="icon-bg bg-pink"></div>
                       </i><span class="menu-title">Profile</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>

                   <ul class="nav nav-second-level">
                       <div class="clearfix"></div>
                       <li>
                           <a href="<?php echo base_url();?>Profile/security"><span class="submenu-title">Security</span></a>
                       </li>

                       <li>
                           <a href="<?php echo base_url();?>Profile/skills"><span class="submenu-title">Skills</span></a>
                       </li>

                       <li>
                           <a href="<?php echo base_url();?>Profile/about"><span class="submenu-title">About</span></a>
                       </li>

                   </ul>
                       
               </li>
                  
                </ul>
            </div>
        </nav>
            <!--END SIDEBAR MENU-->