    <footer class="footer">
            <div class="footer-nav pull-right">
                <ul class="pull-right">
                     <li class="footer-nav-item">
                        <a href=""  target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i>
                        </a>
                     </li>
                  <li class="footer-nav-item">
                  <a href="https://www.facebook.com/fardeen.hakimi"  target="_blank">
                    <i class="fa fa-facebook fa-lg" aria-hidden="true"></i>
                   </a>
                  </li>
                  <li class="footer-nav-item">
                    <a href=""  target="_blank">
                        <i class="fa fa-linkedin fa-lg" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
            </div>
        </footer>  
     
  <script src="<?php echo base_url();?>assets/js/custom.js" type="text/javascript">
  </script>
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js" type="text/javascript">
</script>
    </body>
</html>