<!--BEGIN TOPBAR-->
 <div id="header-topbar-option" class="page-header-topbar">
            <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a id="logo" href="index.html" class="navbar-brand"><span class="fa fa-rocket"></span><span class="logo-text">KAdmin</span><span style="display: none" class="logo-text-icon">µ</span></a></div>
            <div class="topbar-main"><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                    <li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i class="fa fa-bell fa-fw"></i><span class="badge badge-green">3</span></a>  
                    </li>
                   <li class="basic-advanced">
                           <input id="toggle-trigger" type="checkbox" data-toggle="toggle">
                    </li>

                    <li class="dropdown topbar-user">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php if(!empty($this->session->userdata['admin']['avatar'])){
                        echo base_url().'assets/images/users/'.$this->session->userdata['admin']['avatar'];}else{
    echo base_url().'assets/images/users/profile-avatar.jpg';}?>" alt="" class="img-responsive img-circle">&nbsp;<span class="hidden-xs"><?php if(isset($this->session->userdata['admin']['userName'])){echo $this->session->userdata['admin']['userName'];}?></span>&nbsp;<span class="caret"></span></a>

    <ul class="dropdown-menu user-sub-menu">
    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
    <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Settings</a></li>
    <li><a href="<?php echo base_url(); ?>Logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</a></li>
    </ul>
                    </li>
                </ul>
            </div>
        </nav>
        </div>
       