</div>

    <!-- Javascript Navigation SideBar -->

    <script async="" src="//www.google-analytics.com/analytics.js"></script>
     <script src="<?php echo base_url();?>assets/js/jquery-migrate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/bootstrap-select.js"></script>
    <script  src="<?php echo base_url();?>assets/js/bootstrap-submenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.menu.js"></script>
    <script src="<?php echo base_url();?>assets/js/pace.min.js"></script>
    <!--CORE JAVASCRIPT-->
    <script src="<?php echo base_url();?>assets/js/data-table.min.js"></script>
    <script type="text/javascript">base_url = '<?=base_url()?>';</script>


    <script>
          (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-145464-12', 'auto');
        ga('send', 'pageview');
        </script>
       
</body>
</html>