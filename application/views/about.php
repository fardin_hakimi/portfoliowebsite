
<div class="about-image-header">


    <div class="about-image-box">
        <?php $src = base_url()."assets/images/".$me->Featured_image; ?>
        <img src="<?php echo $src; ?>" class="about-image">
    </div>

</div>
            <div class="about-box">


                <div class="content-box-about container">


                <div class="col-lg-6  col-xs-12">
                    <p class="about-me">
                        <?php echo $me->About; ?>
                    </p>

                </div>
                <div class="col-lg-5  col-xs-12 pull-right">
                     <h2>My skills</h2>
                    <?php
                    foreach ($skills as $skill){
                        $src = base_url()."assets/images/".$skill->Featured_image;
                        ?>


                     <div class='skill-box'>

                         <span> <?php echo "<img class='skill-thumb' src='$src' "; ?> </span>
                         <p class="skill-description">
                             <?php echo $skill->Description; ?>
                         </p>

                        </div>

                    <?php
                    }
                    ?>
                </div>
                </div>
            </div>
        </div>