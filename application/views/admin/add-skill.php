<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Dashboard</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Dashboard</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <div class="clearfix"></div>
    <div class="page-content">

        <form action="<?php echo base_url();?>Profile/create_skill" method="post" enctype="multipart/form-data">
            <div class="col-lg-12">

                <!-- skill Id hidden input -->

                <input type="hidden" name="skillId" value="<?php if(isset($skill->Id) && $skill->Id!= '') { echo $skill->Id; } ?>">
                <div class="input-group col-lg-12">
                    <label>Title</label>
                    <input class="contact-input" type="text" name="skillTitle" id="skillTitle"
                           value="<?php if(isset($skill->Name) && $skill->Name != '') { echo $skill->Name; } ?>">
                </div>

                <div class="input-group col-lg-12">
                    <label>Description</label>
                    <textarea rows="5" class="contact-message" name="description" id="description">
               <?php if(isset($skill->Description) && $skill->Description != '') { echo $skill->Description;}?>
                </textarea>
                </div>

                <div class="input-group col-lg-12">
                    <label>Featured Image</label>
                    <input type="file" name="featuredImage" id="featuredImage">
                    <p><?php if(isset($skill->Featured_image)){ echo $skill->Featured_image; }?></p>
                </div>

                <div class="input-group col-lg-12" style="margin-top: 2%; margin-bottom: 10%;">
                    <input class="submit-contact col-lg-3" type="submit" name="submit-skill" id="submit-skill" value="<?php if(isset($skill->Id)){ echo "Edit"; }else{ echo "Add";}?>">
                </div>
            </div>
        </form>
    </div>
    <!--END PAGE WRAPPER-->
</div>