
<div id="page-wrapper">
 <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Dashboard</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Dashboard</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
               <div class="clearfix"></div>
                <div class="page-content">

<form action="<?php echo base_url();?>work/create_work" method="post" enctype="multipart/form-data">
    <div class="col-lg-12">

    <!-- work Id hidden inpu -->

    <input type="hidden" name="workId" value="<?php if(isset($work->Id) && $work->Id != '') { echo $work->Id; } ?>">
    <div class="input-group col-lg-12">
    <label>Title</label>
   <input class="contact-input" type="text" name="workTitle" id="workTitle"
    value="<?php if(isset($work->Title) && $work->Title != '') { echo $work->Title; } ?>">
    </div>
    <div class="input-group col-lg-12" style="margin-bottom: 2%;">
    <label>Category</label>

      <div class="col-lg-12 no-padding"> 
     <?php 
       $category_mod = 'id="category" class="form-control contact-input"';

   /* set the selected if work_id is passed */
      $selected ="";
        if(isset($work->Category_id)){
        $selected = $work->Category_id;
       }

   /* name, passed array, selected item, created mod */

       echo form_dropdown("category",$categories, $selected ,$category_mod);
       ?>

      </div>

      </div>
      <div class="input-group col-lg-12">
      <label>Description</label>
      <textarea rows="5" class="contact-message" name="description" id="description">
       <?php if(isset($work->Description) && $work->Description != '') { echo $work->Description;}?>
      </textarea>
      </div> 

       <div class="input-group col-lg-12">
      <label>Featured Image</label>
      <input type="file" name="featuredImage" id="featuredImage">
      <p><?php if(isset($work->Featured_image)){ echo $work->Featured_image; }?></p>
      </div> 

      <div class="input-group col-lg-12" style="margin-top: 2%; margin-bottom: 2%;">
      <label>Content</label>
      <textarea class="ckeditor" name="content" id="content">
        <?php if(isset($work->Content) && $work->Content != '') { echo $work->Content;}?>
      </textarea>
      </div>
      <div class="input-group col-lg-12" style="margin-top: 2%; margin-bottom: 10%;">
      <input class="submit-contact col-lg-3" type="submit" name="submit-work" id="submit-word" value="<?php if(isset($work->Id)){ echo "Edit"; }else{ echo "Add";}?>">
      </div>
      </div>
        </form>
        </div>


 <!--END PAGE WRAPPER-->
        </div>