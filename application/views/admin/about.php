

<div id="page-wrapper">
 <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
Dashboard</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Dashboard</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
               <div class="clearfix"></div>
                <div class="page-content">


                <?php
                if(validation_errors()){
                    echo "<div class='alert alert-danger' role='alert'> <b>Error !</b><span>".validation_errors()."</span></div>";
                }
                  ?>

<form action="<?php echo base_url();?>profile/update_about" method="post" enctype="multipart/form-data">
    <div class="col-lg-12">

        <div class="input-group col-lg-12">
            <label>Featured Image</label>
            <input type="file" name="featuredImage" id="featuredImage">
            <p><?php if(isset($Admin->Featured_image)){ echo $Admin->Featured_image; }?></p>
        </div>


        <div class="input-group col-lg-12" style="margin-top: 2%; margin-bottom: 2%;">
            <label>About me</label>

        <textarea class="ckeditor" name="content" id="content">
        <?php if(isset($Admin->About) && $Admin->About != '') { echo $Admin->About;}?>
        </textarea>

          <script>
                CKEDITOR.replace('content');

                     </script>

        </div>


        <div class="input-group col-lg-12" style="margin-top: 2%; margin-bottom: 10%;">
            <input class="submit-contact col-lg-3" type="submit" name="update-about" id="update-about" value="Update">
        </div>

    </div>

</form>
</div>


<!--END PAGE WRAPPER-->
</div>



