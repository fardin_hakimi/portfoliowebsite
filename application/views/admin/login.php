
<!DOCTYPE html>
<html>
<head>
	<title>Admin Login Panel</title>
</head>
<body>

<div class="col-lg-6 login-box">
<h2> Admin Login Panel </h2>




<form action ="<?php echo base_url();?>admin/authorizeUser" method="post">

<?php if($this->session->flashdata('msg')) {?>

<div class="alert alert-danger" role="alert">
<b>Error !</b>

<?php echo $this->session->flashdata('msg');?></div><?php }
   if(form_error('emailAddress') && !form_error('password'))
   	{ ?>
  <div class="alert alert-danger" role="alert"><b>Error !</b> <?php echo form_error('emailAddress');  ?>
  </div>

 <?php }else if(form_error('password') && !form_error('emailAddress') )
 {?>

  <div class="alert alert-danger" role="alert"><b>Error !</b> <?php echo form_error('password');  ?>
  </div>
  <?php }else if(form_error('emailAddress') && form_error('password')) {?>
  
  <div class="alert alert-danger" role="alert"><b>Error !</b> Email Adrress or Password is incorrect, Please check your credentials and try again.</div>

                   <?php }?>

  <div class="form-group">
    <label for="Username">Username:</label>
    <input type="email" class="contact-input" name="emailAddress" id="emailAddress">
  </div>
  <div class="form-group">
    <label for="password">Password:</label>
    <input type="password" class="contact-input" name="password" id="password">
  </div>
  <input type="submit" name="signin" id="signin" class="col-lg-3 submit-contact" value="Login">
</form>
</div>

</body>
</html>
