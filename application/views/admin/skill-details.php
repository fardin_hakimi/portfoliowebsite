<div id="page-wrapper">

    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb pages_crumb">
        <div class="page-header pull-left">
            <div class="page-title">
                <h1>Add Work </h1></div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="DashboardController">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="DashboardController">Dashbaord</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="<?php echo base_url();?>profile/skills">Skills</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">View Work</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="clearfix"></div>
    <div class="page-content">
        <div id="tab-general">
            <div class="row">
                <div class="col-xs-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Skill Details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="view">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Work ID:</td>
                                        <td><?php echo $row->Id; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Title:</td>
                                        <td> <?php echo $row->Name; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Description:</td>
                                        <td><?php echo $row->Description; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Thumbnail:</td>
                                        <td>
                                            <img src="<?php echo base_url()."assets/images/".$row->Featured_image;?>" class="work-thumb">


                                        </td>
                                    </tr>



                                    </tbody></table>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


            <!--END TAB GENERAL-->
        </div>
        <!--END PAGE CONTENT-->
    </div>
    <!--END PAGE WRAPPER-->
</div>
