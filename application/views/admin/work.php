<div id="page-wrapper">

                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb
                pages_crumb">
                  <div class="page-header pull-left">
                        <div class="page-title">
                              <h1 class="list"><i class="fa fa-list fa-fw" style="margin-top:-4px;"></i> Works List</h1></div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="Dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li><i class="fa fa-tachometer fa-fw"></i>&nbsp;<a href="Dashboard">Dashbaord</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Works Management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Works Management</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="clearfix"></div>
                <div class="page-content">
                <div id="tab-general">

                <div class="row mbl">
                <div class="col-sm-12">

                <div class="box">
                <div class="box-header">
                 <div  style="float:right;">
                      <a href="<?php echo base_url();?>Work/add_work" class="btn btn-success btn-sm" style="font-weight:bold !important;">Add Work</a>

                  <input type="button" class="btn btn-danger btn-sm" style=font-weight:bold !important;"  id="deleteWork" name="deleteWork" value="Delete">
                 </div>
             
                 </div>
            <!-- /.box-header -->

 <div class="box-body">
          
      
             <div class="row"><div class="col-sm-12"><table id="workList" class="table table-bordered table-striped" role="grid" aria-describedby="">
               
                <thead>
                <tr>
                  <th class="text-center"><input type="checkbox" class="icheck-check-btn checkbox" id="selectAll"></th>
                       
                         <th class="text-center">Title</th>
                         <th class="text-center">Description</th>
                         <th class="text-center">Category</th>
                         <th class="text-center">Status</th>
                         <th class="text-center">Actions</th></tr>
                </thead>
                <tbody class="tbodyhide">

                 <?php 
                 if(!empty($works)){
                  foreach ($works as $row) { ?>
                       <tr>


    <td align="center"><input type="checkbox" value="<?php echo $row->work_id; ?>" id="checkbox" class="icheck-check-btn checkbox" name="userId[]"></td>
    
      <td align="center"><?php echo $row->Title; ?></td>
      <td align="center"><?php echo $row->work_description ?></td>
      <td align="center"><?php echo $row->Name; ?></td>

    <td align="center">

    <a href="" id="<?php if( $row->work_status == 1 ) { echo $row->work_id.",".$row->work_status.'" style="color:#337ab7;" '; }else{ echo  $row->work_id.",".$row->work_status.'" style="color:red;" '; } ?>class="work_status">

     <?php if($row->work_status == 1)
     { echo "Published";}else{ echo "Publish";} ?>
     </a>

    </td>

     <td align="center">
     <a href="<?php echo base_url();?>work/view_work/<?php echo $row->work_id; ?>" id="<?php echo $row->work_id; ?>"><span class="fa fa-eye green"></span>
     </a>
  

      <a href="<?php echo base_url();?>work/edit_work/<?php echo $row->work_id; ?>" id="<?php echo $row->work_id; ?>"><span class="fa fa-pencil blue"></span>
      </a>
   

     
      <a href="<?php echo base_url();?>work/delete_work/<?php echo $row->work_id; ?>" id="<?php echo $row->work_id; ?>"><span class="fa fa-trash-o red"></span>
      </a>
      </td>
      </tr>

        <?php }

         }else { echo ' <tr>
            <td class="text-center" colspan="11">No results!</td>
                            </tr>';} 
         ?>
          
          </tbody><tbody class="tbodyshow"></tbody>
                
              </table></div></div></div>
            </div>
            <!-- /.box-body -->

                   </div> <!--END BOX-->
                    </div> <!--END col-sm-12-->
                   </div> <!--END ROW MBL-->
                 <!--END PAGE CONTENT-->
                </div>
                 <!--END TAB GENERAL-->
                </div>
  <!--END PAGE WRAPPER-->
 </div>

<script type="text/javascript">


$(document).ready(function() {
    $('#workList').DataTable();
} );


  $(document).on('click', '.work_status', function(e){

     var id = this.id;
     var result = id.split(",");

   e.preventDefault();
         $.ajax({

            type: "POST",
            url: "Work/update_status",
            data: {  
               "workId" :result[0],
               "Status":result[1]
             } ,
          success: function(data) {
          window.location = "Work";
            }
          });
 });

</script>
 