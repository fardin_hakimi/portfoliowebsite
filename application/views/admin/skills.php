<div id="page-wrapper">

    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb
                pages_crumb">
        <div class="page-header pull-left">
            <div class="page-title">
                <h1 class="list"><i class="fa fa-list fa-fw" style="margin-top:-4px;"></i> Skills List</h1></div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="Dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><i class="fa fa-tachometer fa-fw"></i>&nbsp;<a href="Dashboard">Dashbaord</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="hidden"><a href="#">Skills Management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Skills Management</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="clearfix"></div>
    <div class="page-content">
        <div id="tab-general">

            <div class="row mbl">
                <div class="col-sm-12">

                    <div class="box">
                        <div class="box-header">
                            <div  style="float:right;">
                                <a href="<?php echo base_url();?>Profile/add_skill" class="btn btn-success btn-sm" style="font-weight:bold !important;">Add Skill</a>
                            </div>

                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">


                            <div class="row"><div class="col-sm-12">
                                    <table id="skillList" class="table table-bordered table-striped" role="grid" aria-describedby="">

                                        <thead>
                                        <tr>
                                            <th class="text-center"><input type="checkbox" class="icheck-check-btn checkbox" id="selectAll"></th>

                                            <th class="text-center">Name</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbodyhide">

                                        <?php
                                        if(!empty($skills)){
                                            foreach ($skills as $row) { ?>
                                                <tr>


                                                    <td align="center"><input type="checkbox" value="<?php echo $row->Id; ?>" id="checkbox" class="icheck-check-btn checkbox" name="userId[]"></td>
                                                    <td align="center"><?php echo $row->Name; ?></td>
                                                    <td align="center"><?php echo $row->Description ?></td>


                                                    <td align="center">
                                                        <a href="<?php echo base_url();?>Profile/view_skill/<?php echo $row->Id; ?>" id="<?php echo $row->Id; ?>"><span class="fa fa-eye green"></span>
                                                        </a>


                                                        <a href="<?php echo base_url();?>Profile/edit_skill/<?php  echo $row->Id; ?>" id="<?php echo $row->Id; ?>"><span class="fa fa-pencil blue"></span>
                                                        </a>

                                                        <a href="<?php echo base_url();?>Profile/delete_skill/<?php  echo $row->Id; ?>" id="<?php echo $row->Id; ?>"><span class="fa fa-trash-o red"></span>
                                                        </a>
                                                    </td>
                                                </tr>

                                            <?php }

                                        }else { echo ' <tr>
                                      <td class="text-center" colspan="11">No results!</td>
                            </tr>';}
                                        ?>

                                        </tbody><tbody class="tbodyshow"></tbody>

                                    </table></div></div></div>
                    </div>
                    <!-- /.box-body -->

                </div> <!--END BOX-->
            </div> <!--END col-sm-12-->
        </div> <!--END ROW MBL-->
        <!--END PAGE CONTENT-->
    </div>
    <!--END TAB GENERAL-->
</div>
<!--END PAGE WRAPPER-->
</div>

<script>

        $(document).ready(function() {
            $('#skillList').DataTable();
        } );
</script>
