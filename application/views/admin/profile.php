
<div id="page-wrapper">
 <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Dashboard</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Dashboard</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
               <div class="clearfix"></div>
                <div class="page-content">


                <?php 
                if(validation_errors()){
                    echo "<div class='alert alert-danger' role='alert'> <b>Error !</b><span>".validation_errors()."</span></div>";
                   }
                  ?>

<form action="<?php echo base_url();?>profile/update_security" method="post" enctype="multipart/form-data">
    <div class="col-lg-12">


    <div class="input-group col-lg-12">
    <label>Username</label>
   <input class="contact-input" type="text" name="userName" id="userName"
    value="<?php if(isset($Admin->User_Name) && $Admin->User_Name != '') { echo $Admin->User_Name; } ?>">
    </div>

    <div class="input-group col-lg-12">
    <label>Password</label>
   <input class="contact-input" type="password" name="password" id="password">
    </div>


      <div class="input-group col-lg-12" style="margin-top: 2%; margin-bottom: 10%;">
      <input class="submit-contact col-lg-3" type="submit" name="update-profile" id="update-profile" value="Update Profile">
      </div>

      </div>

        </form>
        </div>


 <!--END PAGE WRAPPER-->
        </div>